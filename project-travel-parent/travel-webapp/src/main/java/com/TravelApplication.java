package com;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by LIXU on 2018/3/19.
 */
@SpringBootApplication(exclude = {HibernateJpaAutoConfiguration.class})
@RestController
public class TravelApplication {

    @RequestMapping("/hello")
    public String hello() {
        return "hello world";
    }


    public static void main(String[] args) {
        SpringApplication.run(TravelApplication.class, args);
    }
}
