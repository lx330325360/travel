package com.travel.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by LIXU on 2018/3/19.
 */
@RestController
public class TestController {
    @RequestMapping("/chat")
    public String test(){
        return "test";
    }
}
